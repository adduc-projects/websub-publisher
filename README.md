WebSub Publisher
===

### Specification

* A conforming publisher MUST advertise topic and hub URLs for a given
  resource URL as described in Discovery.
* The publisher MUST inform the hubs it previously designated when a topic has been updated. The hub and the publisher can agree on any mechanism, as long as the hub is eventually able send the updated payload to the subscribers.

#### Note

The specific mechanism for the publisher to inform the hub is left unspecified. For example, some existing public hubs [1] [2] [3] ask publishers to send a POST request with the keys hub.mode="publish" and hub.url=(the URL of the resource that was updated).
