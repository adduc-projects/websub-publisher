<?php declare(strict_types=1);

namespace Adduc\WebSub;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;
use Psr\Log\NullLogger;
use Psr\Http\Message\UriInterface;

class Publisher
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var Client */
    protected $client;

    public function __construct(?LoggerInterface $logger = null, ?Client $client = null)
    {
        if ($logger === null) {
            $logger = new NullLogger();
        }

        if ($client === null) {
            $client = new Client();
        }

        $this->logger = $logger;
        $this->client = $client;
    }

    public function publish(UriInterface $hub, UriInterface $topic): void
    {
        $this->client->post($hub, [
            'form_params' => [
                'hub.mode' => 'publish',
                'hub.url' => $topic->__toString()
            ]
        ]);
    }
}
