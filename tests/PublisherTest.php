<?php declare(strict_types=1);

namespace Adduc\WebSub;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    /** @var array */
    protected $history = [];

    /**
     * @dataProvider providePublish
     */
    public function testPublish(Uri $hub, Uri $topic): void
    {
        $client = $this->getClient(new Response(204));

        $publisher = new Publisher(null, $client);
        $publisher->publish($hub, $topic);

        $this->assertEquals($hub, $this->history[0]['request']->getUri());
    }

    public function providePublish(): array
    {
        $uris = [
            new Uri('https://example.com/1'),
            new Uri('https://example.com/2'),
        ];

        $tests = [
            [$uris[0], $uris[1]],
            [$uris[1], $uris[0]],
        ];

        return $tests;
    }

    protected function getClient(Response ...$responses): Client
    {
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);

        $history = Middleware::history($this->history);
        $handler->push($history);

        $client = new Client(['handler' => $handler]);

        return $client;
    }
}
